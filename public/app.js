let duracionMinutos;
let segundos = 60;
let actividadTexto;
let id;

let hora;
let minutos;
let seg;

const horaActual = document.querySelector(".hour");

const temporizador = document.querySelector(".temporizator");
const btnReanudar = document.querySelector(".boton-reanudar");
const article = document.querySelector("article");
const otraClase = document.querySelector(".otraClase");
const parrafoActividad = document.createElement("p");
const btnPausar = document.querySelector(".boton-pausar");
const btnLimpiar = document.querySelector(".boton-limpiar");
const duracion = document.querySelector("#duracion");
const actividad = document.querySelector("#actividad");

const textoActividad = document.querySelector(".text-activity");

document.addEventListener("DOMContentLoaded", funFecha);

function funFecha(){

    const diaHoy = new Date();

    let diaSemana= diaHoy.getDay();
    let numeroMes= diaHoy.getDate();
    let mes= diaHoy.getMonth();
    let year = diaHoy.getFullYear();


    switch(diaSemana){
        case 0: diaSemana = "Domingo";
        break;
        case 1: diaSemana = "Lunes";
        break;
        case 2: diaSemana = "Martes";
        break;
        case 3: diaSemana = "Miercoles";
        break;
        case 4: diaSemana = "Jueves";
        break;
        case 5: diaSemana = "Viernes";
        break;
        case 6: diaSemana = "Sabado";
        break;
    }

    switch(mes){
        case 0: mes = "Enero";
        break;
        case 1: mes = "Febrero";
        break;
        case 2: mes = "Marzo";
        break;
        case 3: mes = "Abril";
        break;
        case 4: mes = "Mayo";
        break;
        case 5: mes = "Junio";
        break;
        case 6: mes = "Julio";
        break;
        case 7: mes = "Agosto";
        break;
        case 8: mes = "Septiembre";
        break;
        case 9: mes = "Octubre";
        break;
        case 10: mes = "Noviembre";
        break;
        case 11: mes = "Diciembre";
        break;
    };

    const datosFecha = `${diaSemana} ${numeroMes} ${mes} ${year}`
    console.log(datosFecha);
    const fecha = document.querySelector(".date").textContent = datosFecha;
};

setInterval(() => {
    const horaActualizada = new Date();
    // console.log(datosHora);
    hora = horaActualizada.getHours();
    minutos = horaActualizada.getMinutes();
    seg = horaActualizada.getSeconds();
    
    datosHora = `${hora}:${minutos}:${seg}`;
    // horaActual = document.querySelector(".hour")
    horaActual.textContent = datosHora;

    if (hora === 0){
        funFecha();
    }

}, 1000);

const btnActividad = document.querySelector(".boton-actividad")
btnActividad.addEventListener("click", (e) => {
    // const actividad = document.querySelector("#actividad");
    console.log("Actividad..");
    console.log(actividad.value);

    actividadTexto = actividad.value;
    btnActividad.disabled = true;
    btnActividad.style.color = "#000000";
    
    // const parrafoActividad = document.createElement("p");
    parrafoActividad.classList.add( "hour");
    parrafoActividad.style.color = "#d45500"
    parrafoActividad.textContent = actividadTexto;
    article.appendChild(parrafoActividad);
});

const btnDuracion = document.querySelector(".boton-duracion");
btnDuracion.addEventListener("click", (e) => {
    // const duracion = document.querySelector("#duracion");
    
    duracion.value = Math.floor(duracion.value)
    console.log(duracion.value);
    // console.log(typeof parseInt(duracion.value), "l")
    // console.log(typeof duracion.value, typeof 1);
    // console.log(typeof duracion.value === !isNaN);
    
    if ( !isNaN(parseInt(duracion.value)) && duracion.value > 0  ){
        duracionMinutos = duracion.value
    }
    
    if (duracionMinutos){

        textoLista = document.createElement("li");
        textoLista.textContent = actividad.value;
        textoActividad.appendChild(textoLista);

        tiempoIniLista = document.createElement("li");
        tiempoIniLista.textContent = `${hora}:${minutos}:${seg}`;
        textoActividad.appendChild(tiempoIniLista);
        
        tiempoIniLista = document.createElement("li");
        tiempoIniLista.textContent = `${hora}:${minutos + parseInt(duracion.value)}:${seg}`;
        textoActividad.appendChild(tiempoIniLista);
        
        duracionLista = document.createElement("li");
        duracionLista.textContent = duracion.value + " min";
        textoActividad.appendChild(duracionLista);
        
        const salto = document.createElement("br");
        textoActividad.appendChild(salto);

        btnDuracion.disabled = true;
        btnDuracion.style.color = "#000000";
        btnReanudar.disabled = true;
        btnReanudar.style.color = "#000000";
        btnPausar.disabled = false;
        btnPausar.style.color = "#ffffff";

        duracionMinutos--;
        id = setInterval(conteoTemporizador, 1000);

        function conteoTemporizador(){
            // const temporizador = document.querySelector(".temporizator");
            segundos--;
            
            if (duracionMinutos <= 0 && segundos <= 0){
                clearInterval(id);
                temporizador.textContent = "00:00";
                segundos = 60;
                // article.removeChild(parrafoActividad);
                parrafoActividad.remove()


                btnDuracion.disabled = false;
                btnDuracion.style.color = "#ffffff";
            
                btnActividad.disabled = false;
                btnActividad.style.color = "#ffffff";

                btnPausar.disabled = true;
                btnPausar.style.color = "#000000";

            } else if (segundos === 0){
                duracionMinutos--;
                segundos = 59;
            } else {

                temporizador.textContent = `${duracionMinutos} : ${segundos}`;
            }
            
            // const parrafoActividad = document.createElement("p");
            // parrafoActividad.classList.add( "hour");
            // parrafoActividad.style.color = "#d45500"
            // parrafoActividad.textContent = actividadTexto;
            // otraClase.appendChild(parrafoActividad)
            
        }

        // const btnPausar = document.querySelector(".boton-pausar");
        btnPausar.addEventListener("click", () => {
            console.log("pausar...");
            clearInterval(id);
            btnReanudar.disabled = false;
            btnReanudar.style.color = "#ffffff";
            btnPausar.disabled = true;
            btnPausar.style.color = "#000000";

        });
        
        // const btnReanudar = document.querySelector(".boton-reanudar");
        btnReanudar.addEventListener("click", () => {
            console.log("reanudar...");
            id = setInterval(conteoTemporizador, 1000);
            btnReanudar.disabled = true;
            btnReanudar.style.color = "#000000";
            
            btnPausar.disabled = false;
            btnPausar.style.color = "#ffffff";
        });

    }
});

btnLimpiar.addEventListener("click", () => {
    console.log("limpiar...");
    duracion.value = "";
    clearInterval(id);
    temporizador.textContent = "00:00"
    parrafoActividad.remove();
    actividad.value = "";

    btnDuracion.disabled = false;
    btnDuracion.style.color = "#ffffff";
            
    btnActividad.disabled = false;
    btnActividad.style.color = "#ffffff";

    btnPausar.disabled = true;
    btnPausar.style.color = "#000000";
});


